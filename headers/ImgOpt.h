#pragma once
#include<string>
#include <vector>
#include <functional>
//common type definitions.
typedef long int int32;
typedef long long int int64;
typedef unsigned char byte;
typedef unsigned int uint;

//declare a namespace to midigate signature collisions.
namespace img_opt {
	//structures defintion
	struct rectangle {
	public:
		int x;
		int y;
		int width;
		int height;
		
		rectangle(int width, int height) : width(width), height(height) { }

		rectangle(int x, int y, int width, int height) : x(x), y(y), width(width), height(height) { }

		bool operator ==(rectangle& rect) {
			return x == rect.x && y == rect.y && width == rect.width && height == rect.height;
		}

		bool operator >=(rectangle& rect) {
			return x >= rect.x && y >= rect.y && width <= rect.width && height <= rect.height;
		}

		bool operator <=(rectangle& rect) {
			return x <= rect.x && y <= rect.y && width >= rect.width && height >= rect.height;
		}

		bool operator >(rectangle& rect) {
			return x > rect.x && y > rect.y && width < rect.width && height < rect.height;
		}

		bool operator <(rectangle& rect) {
			return x < rect.x && y < rect.y && width > rect.width && height > rect.height;
		}

		std::string operator+(std::string s) {
			return s + str();
		}

		const char* operator+(char* s) {
			return (str() + s).c_str();
		}

		std::string str();
	};

	struct image {
	private:
		byte * data;
		bool ownership;
	public:
		rectangle view;
		//Constructor for image that takes the width and height.
		image(int width, int height) : data(new byte[width * height]), view({ width = width, height = height }), ownership(true) { }

		//Constructor for image that takes the data, width and height.
		image(byte* data, int width, int height) : data(data), view(width, height) { }
		~image();
		void colour_cvt(image& dst, rectangle rect);
		template<typename t> t* getData() {
			static_assert(std::is_integral_v<t>, "t is valid");
			return (t*)data;
		}

		//a way to access the image as an array.
		byte* operator->() {
			return data;
		}

		template<typename t> t& operator[](int i) {
			static_assert(std::is_integral_v<t>, "t is valid");
			return data[i];
		}
		
		//template<typename t> t& operator[](int i, int j) {
		//	static_assert(std::is_integral_v<t>, "t is valid");
		//	return data[i + j * view.width];
		//}

		std::vector<byte> getCropped();
	};

	//function definition.
	bool is_little_endian();

	bool not_all(std::initializer_list<int> l);
	
	void rgb2bgr_opt_le(image& source, image& target, rectangle dst_area);

	void rgb2bgr_opt_be(image& source, image& target, rectangle dst_area);

	void colour_cvt(image& source, image& target, rectangle area);
	
	std::string operator+(std::string t, rectangle r);
}