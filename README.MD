# Colour converter 
## decription
Colour Converter is a c++ library centered around conversion from the BGR colourspace to RGB colourspace and the counterway.
The emphisis in this repository is on performance, using [bit twiddling](https://en.wikipedia.org/wiki/Bit_manipulation) , minimal memory accesses([virtuallization is a hit for performance](https://en.wikipedia.org/wiki/Virtual_memory))
and exploiting of architecture specific structure.

## Why would I use it?
Although it's one of many it's an extremely straitfoward and targeted codebase meaning it's easier to learn use and modify for your need.
In addition it's one of the fastest implementations(without GPU).

## Objectives
The general the intent is to make the library simple, easy to use and fast.
* In addition it should be well-written(idiomatic) and stylized.
* It should be easy to understand.
* When calling a function you shouldn't need to clean-up.
 
## Structure 
Colour Converter contains a structure called image and utitlity functions for colour convertion revolving around it.
A good start in the repository is source/ImgOpt.cpp#image::cvtColour.

## Compiling
to compile this you need a compiler that supports c++ 11 and add /headers to the includes path.
for example for in g++ the command will look like:
g++ -std=c++11 -Wall -Werror -I/headers "/source/*.cpp" -fPIC -shared -o colour_cvt.so