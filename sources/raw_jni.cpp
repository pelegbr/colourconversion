#include "stdafx.h"
#include "ImgOpt.h"
#include <iostream>
#include <sstream>
#include "java/jvmti.h"

//the jni/raw parallel
#define jmethod(i) JNIEXPORT i JNICALL
#define jvoid jmethod(void)
#define jstatic_params JNIEnv* env, jclass cls
#define jparams JNIEnv* env, jobject self

#define util_func(T,...) \
	Java_com_sp_guip_img_imgUtil_##T(jstatic_params, __VA_ARGS__)


/**
* This function is a utility to query wether the running pc is little or big endian(https://en.wikipedia.org/wiki/Endianness)
* @returns true for little endian or false for big endian.
*/
bool is_little_endian() {
	union {
		int i;
		char c[4];
	} b;
	b.i = 1;
	if (b.c[0] == 1) return true;
	return false;
}


/**
* This function is an optimized colour converter for when the running pc is LittleEndian, if uncertian call the father function #cvtColour(byte*, byte*, int, int, int, int, int, int)
* The function exploits bit twiddling(https://en.wikipedia.org/wiki/Bit_manipulation) of a 3channel image to 4byte integers for less and larger memory accesses leading to much faster performance.
* altough much faster this algorithm is not generic applies only to an image that fulfill width % 4 == 0 && tw % 4 == 0 because of the twiddling of 3 byte figures to 4 byte figures.
* @param width the width of the data buffer(source image).
* @param tx the x cordinate of the region converted in the source image.
* @param ty the y cordinate of the region converted in the source image.
* @param tw the width of the region converted in the source image.
* @param th the height of the region converted in the source image.
*/
void rgb2bgr_opt_le(uint* data, uint* oData, int width, int tx, int ty, int tw, int th) {
	//pos: the position of the source image.
	register uint* pos = data + (tx + ty * width) / 4 * 3;
	//pos: the position of the target image.
	register uint* new_pos = oData;
	//int_width: the number of memory accesses for each line(width) in the source image.
	uint int_width = width / 4 * 3;
	//t_int_width: the number of memory accesses for each line(length of tw instead of width).
	uint t_int_width = tw / 4 * 3;
	//img_end: the end position of the buffer(source image)
	uint* img_end = pos + int_width * th;
	//end: the end of the current line in the source image.
	register uint* end;
	while (pos != img_end) {
		end = pos + t_int_width;
		//next_a: the next line in the source image.
		uint* next_a = pos + int_width;
		//next_n: the next line in the target image.
		uint* next_n = new_pos + t_int_width;
		//while the current line still has pixles to convert
		while (pos != end) {
			//query 12 byte.
			//cur: bytes 0-4
			uint cur = *(pos++);
			//next: bytes 4-8
			uint next = *(pos++);
			//nn: bytes 8-12
			uint nn = *(pos++);
			//the following diagrams explain the bit twiddling addressing what origin of byte goes where.
			//[origin(0-12)] -> [distination(0-12)]
			//[2] -> [0]
			//[1] -> [1]
			//[0] -> [2]
			//[5] -> [3]
			*new_pos = (cur & 0xFF0000) >> 16 | (cur & 0xFF00) | (cur & 0xFF) << 16 | (next & 0xFF00) << 16;
			new_pos++;
			//[4] -> [4]
			//[3] -> [5]
			//[8] -> [6]
			//[7] -> [7]
			*new_pos = (next & 0xFF000000) | (cur & 0xFF000000) >> 16 | (nn & 0xFF) << 16 | (next & 0xFF);
			new_pos++;
			//[6] -> [8]
			//[11] -> [9]
			//[10] -> [10]
			//[9] -> [11]
			*new_pos = (next & 0xFF0000) >> 16 | (nn & 0xFF000000) >> 16 | (nn & 0xFF0000) | (nn & 0xFF00) << 16;
			new_pos++;
		}
		//cleanup.
		//go to the next line of source and target.
		pos = next_a;
		new_pos = next_n;
	}
}

/**
* This function is an optimized colour converter for when the running pc is BigEndian, if uncertian call the father function #cvtColour(byte*, byte*, int, int, int, int, int, int)
* The function exploits bit twiddling(https://en.wikipedia.org/wiki/Bit_manipulation) of a 3channel image to 4byte integers for less and larger memory accesses leading to much faster performance.
* altough much faster this algorithm is not generic applies only to an image that fulfill width % 4 == 0 because of the twiddling of 3 byte figures to 4 byte figures.
* @param width the width of the data buffer(source image).
* @param tx the x cordinate of the region converted in the source image.
* @param ty the y cordinate of the region converted in the source image.
* @param tw the width of the region converted in the source image.
* @param th the height of the region converted in the source image.
*/
void rgb2bgr_opt_be(uint* data, uint*  oData, int width, int tx, int ty, int tw, int th) {
	//pos: the position of the source image.
	register uint* pos = data + (tx + ty * width) / 4 * 3;
	//pos: the position of the target image.
	register uint* new_pos = oData;
	//int_width: the number of memory accesses for each line(width) in the source image.
	uint int_width = width / 4 * 3;
	//t_int_width: the number of memory accesses for each line(length of tw instead of width).
	uint t_int_width = tw / 4 * 3;
	//img_end: the end position of the buffer(source image)
	uint* img_end = pos + int_width * th;
	//end: the end of the current line in the source image.
	register uint* end;
	while (pos != img_end) {
		end = pos + t_int_width;
		//next_a: the next line in the source image.
		uint* next_a = pos + int_width;
		//next_n: the next line in the target image.
		uint* next_n = new_pos + t_int_width;
		//while the current line still has pixles to convert
		while (pos != end) {
			//query 12 byte.
			//cur: bytes 0-4
			uint cur = *(pos++);
			//next: bytes 4-8
			uint next = *(pos++);
			//nn: bytes 8-12
			uint nn = *(pos++);
			//the following diagrams explain the bit twiddling addressing what origin of byte goes where.
			//[origin(0-12)] -> [distination(0-12)]
			//[2] -> [0]
			//[1] -> [1]
			//[0] -> [2]
			//[5] -> [3]
			*new_pos = (cur & 0xFF00) << 16 | (cur & 0xFF0000) | (cur & 0xFF000000) >> 16 | (next & 0xFF0000) >> 16;
			new_pos++;
			//[4] -> [4]
			//[3] -> [5]
			//[8] -> [6]
			//[7] -> [7]
			*new_pos = (next & 0xFF) | (cur & 0xFF) << 16 | (nn & 0xFF000000) >> 16 | (next & 0xFF000000);
			new_pos++;
			//[6] -> [8]
			//[11] -> [9]
			//[10] -> [10]
			//[9] -> [11]
			*new_pos = (next & 0xFF00) << 16 | (nn & 0xFF) << 16 | (nn & 0xFF00) | (nn & 0xff0000) >> 16;
			new_pos++;
			new_pos++;
			pos++;
		}
		//cleanup.
		//go to the next line of source and target.
		pos = next_a;
		new_pos = next_n;
	}
}

/**
* This function converts a region of an image from RGB->BGR and the opposite(originally meant to work alongside java-jni #L191).
* @param image is the source image to copy and convert to RGB/BGR.
* @param target is the target image to copy the source to.
* @param width the width of the source image.
* @param height the height of the source image.
* @param tx the x cordinate of the region converted in the source image.
* @param ty the y cordinate of the region converted in the source image.
* @param tw the width of the region converted in the source image.
* @param th the height of the region converted in the source image.
*/
void colourCvt(byte* source, byte* target, int32 width, int32 height, int32 tx, int32 ty, int tw, int th) {
	//condition for optimized convertion @see #rgb2bgr_opt_le(uint*, uint*,int32, int32, int32, int32, int32) or #rgb2bgr_opt_be(uint*, uint*,int32, int32, int32, int32, int32).
	if (tw % 4 == 0 && width % 4 == 0) {
		if (is_little_endian())
			rgb2bgr_opt_le((uint*)source, (uint*)target, width, tx, ty, tw, th);
		else
			rgb2bgr_opt_be((uint*)source, (uint*)target, width, tx, ty, tw, th);
	}
	else {
		//optimized generic algorithm.
		//put all variables to the register for immiadate access in cpu.
		register byte* data = source;
		register byte* oData = target;
		//index: the position in the source image.
		register uint index = (tx + ty * width) * 3;
		//new_index: the position in the target image.
		register uint new_index = 0;
		//image_end: the position to stop converting.
		uint image_end = index + width * th * 3;
		//xend: the end position of the current line.
		register uint xend;
		//while there is still data to convert.
		while (index < image_end) {
			//copy the whole line to the target image.
			memcpy(oData + new_index, data + index, tw * 3);
			//set the end position of this line.
			xend = index + tw * 3;
			//the next line in the source image.
			int next_i = index + width * 3;
			//the next line in the target image.
			int next_ni = new_index + tw * 3;
			//while there is still data to copy in this line.
			while (index < xend) {
				//flip the first and third channel.
				oData[new_index++] = data[index + 2];
				new_index++;
				oData[new_index++] = data[index];
				index += 3;
			}
			//cleanup.
			//go to the next line of source and target.
			index = next_i;
			new_index = next_ni;
		}
	}
}


//the jni version of the function.
void util_func(colourCvt, jbyteArray image, jbyteArray oImage, jint width, jint height, jint tx, jint ty, jint tw, jint th) {
	if (tw % 4 == 0) {
		uint* data = (uint*)env->GetPrimitiveArrayCritical(image, NULL);
		uint* oData = (uint*)env->GetPrimitiveArrayCritical(oImage, NULL);
		if (is_little_endian())
			rgb2bgr_opt_le(data, oData, width, tx, ty, tw, th);
		else
			rgb2bgr_opt_be(data, oData, width, tx, ty, tw, th);
		env->ReleasePrimitiveArrayCritical(image, data, JNI_ABORT);
		env->ReleasePrimitiveArrayCritical(oImage, oData, JNI_COMMIT);
	}
	else {
		register char* data = (char*)env->GetPrimitiveArrayCritical(image, NULL);
		register char* oData = (char*)env->GetPrimitiveArrayCritical(oImage, NULL);
		register uint index = (tx + ty * width) * 3;
		register uint new_index = 0;
		uint yend = index + width * th * 3;
		register uint xend;
		while (index < yend) {
			memcpy(oData + new_index, data + index, tw * 3);
			xend = index + tw * 3;
			int next_i = index + width * 3, next_ni = new_index + tw * 3;
			while (index < xend) {
				oData[new_index++] = data[index + 2];
				new_index++;
				oData[new_index++] = data[index];
				index += 3;
			}
			index = next_i, new_index = next_ni;
		}
		env->ReleasePrimitiveArrayCritical(image, data, JNI_ABORT);
		env->ReleasePrimitiveArrayCritical(oImage, oData, JNI_COMMIT);
	}
}