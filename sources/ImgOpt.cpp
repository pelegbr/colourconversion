#include "stdafx.h"
#include "ImgOpt.h"
#include <iostream>
#include <sstream>
#include<functional>

namespace img_opt {
	/**
	* This function is a utility to query wether the running pc is little or big endian(https://en.wikipedia.org/wiki/Endianness)
	* @returns true for little endian or false for big endian.
	*/
	bool is_little_endian() {
		union {
			int i;
			char c[4];
		} b;
		b.i = 1;
		if (b.c[0] == 1) return true;
		return false;
	}

	/**
	* This is a utility function to check all conditions given to it are false.
	* @returns whether all conditions given are false.
	*/
	bool not_all(std::initializer_list<int> l) {
		for (int i : l)
			if (i) return false;
		return true;
	}


	/**
	* This function is an optimized colour converter for when the running pc is LittleEndian, if uncertian call the father function #cvtColour(byte*, byte*, int, int, int, int, int, int)
	* The function exploits bit twiddling(https://en.wikipedia.org/wiki/Bit_manipulation) of a 3channel image to 4byte integers for less and larger memory accesses leading to much faster performance.
	* altough much faster this algorithm is not generic applies only to an image that fulfill width % 4 == 0 && tw % 4 == 0 because of the twiddling of 3 byte figures to 4 byte figures.
	* @param source the image to convert.
	* @param target the image to put the results in.
	* @param dst_area the area in the target
	*/
	void rgb2bgr_opt_le(image& source, image& target, rectangle dst_area) {
		//pos: the position of the source image.
		register uint* pos = source.getData<uint>() + (source.view.x + source.view.y * source.view.width) / 4 * 3;
		//pos: the position of the target image.
		register uint* new_pos = target.getData<uint>() + (target.view.x + dst_area.x + (target.view.y + dst_area.y) * target.view.width) / 4 * 3;
		//all vars coming up with int in the name suggest the conversion of 3 byte co-ordinates to 4 byte cordinates(n / 4 * 3).
		//int_width: the number of memory accesses for each line(width) in the source image.
		uint int_width = source.view.width / 4 * 3;
		//t_int_width: the number of byte*4 for each line(width) in the target image.
		uint t_int_width = target.view.width / 4 * 3;
		//dst_int_width: the number of memory accesses for each line(length of tw instead of width)
		uint dst_int_width = dst_area.width / 4 * 3;
		//img_end: the end position of the buffer(source image)
		uint* img_end = pos + int_width * dst_area.height;

		//end_line: the end of the current line in the source image.
		register uint* end_line;
		while (pos != img_end) {
			end_line = pos + dst_int_width;
			//next_a: the next line in the source image.
			uint* next_a = pos + int_width;
			//next_n: the next line in the target image.
			uint* next_n = new_pos + t_int_width;
			//while the current line still has pixles to convert
			while (pos != end_line) {
				//query 12 byte.
				//cur: bytes 0-4
				uint cur = *(pos++);
				//next: bytes 4-8
				uint next = *(pos++);
				//nn: bytes 8-12
				uint nn = *(pos++);
				//the following diagrams explain the bit twiddling addressing what origin of byte goes where.
				//[origin(0-12)] -> [distination(0-12)]
				//[2] -> [0]
				//[1] -> [1]
				//[0] -> [2]
				//[5] -> [3]
				*new_pos = (cur & 0xFF0000) >> 16 | (cur & 0xFF00) | (cur & 0xFF) << 16 | (next & 0xFF00) << 16;
				new_pos++;
				//[4] -> [4]
				//[3] -> [5]
				//[8] -> [6]
				//[7] -> [7]
				*new_pos = (next & 0xFF000000) | (cur & 0xFF000000) >> 16 | (nn & 0xFF) << 16 | (next & 0xFF);
				new_pos++;
				//[6] -> [8]
				//[11] -> [9]
				//[10] -> [10]
				//[9] -> [11]
				*new_pos = (next & 0xFF0000) >> 16 | (nn & 0xFF000000) >> 16 | (nn & 0xFF0000) | (nn & 0xFF00) << 16;
				new_pos++;
			}
			//cleanup.
			//go to the next line of source and target.
			pos = next_a;
			new_pos = next_n;
		}
	}


	/**
	* This function is an optimized colour converter for when the running pc is BigEndian, if uncertian call the father function #cvtColour(byte*, byte*, int, int, int, int, int, int)
	* The function exploits bit twiddling(https://en.wikipedia.org/wiki/Bit_manipulation) of a 3channel image to 4byte integers for less and larger memory accesses leading to much faster performance.
	* altough much faster this algorithm is not generic applies only to an image that fulfill width % 4 == 0 because of the twiddling of 3 byte figures to 4 byte figures.
	* @param source the image to convert.
	* @param target the image to put the results in.
	* @param dst_area the area in the target
	*/
	void rgb2bgr_opt_be(image& source, image& target, rectangle dst_area) {
		//pos: the position of the source image.
		register uint* pos = source.getData<uint>() + (source.view.x + source.view.y * source.view.width) / 4 * 3;
		//pos: the position of the target image.
		register uint* new_pos = target.getData<uint>() + (target.view.x + dst_area.x + (target.view.y + dst_area.y) * target.view.width) / 4 * 3;
		//all vars coming up with int in the name suggest the conversion of 3 byte co-ordinates to 4 byte cordinates(n / 4 * 3).
		//int_width: the number of memory accesses for each line(width) in the source image.
		uint int_width = source.view.width / 4 * 3;
		//t_int_width: the number of byte*4 for each line(width) in the target image.
		uint t_int_width = target.view.width / 4 * 3;
		//dst_int_width: the number of memory accesses for each line(length of tw instead of width)
		uint dst_int_width = dst_area.width / 4 * 3;
		//img_end: the end position of the buffer(source image)
		uint* img_end = pos + int_width * dst_area.height;

		//end_line: the end of the current line in the source image.
		register uint* end_line;
		while (pos != img_end) {
			end_line = pos + dst_int_width;
			//next_a: the next line in the source image.
			uint* next_a = pos + int_width;
			//next_n: the next line in the target image.
			uint* next_n = new_pos + t_int_width;
			//while the current line still has pixles to convert
			while (pos != end_line) {
				//query 12 byte.
				//cur: bytes 0-4
				uint cur = *(pos++);
				//next: bytes 4-8
				uint next = *(pos++);
				//nn: bytes 8-12
				uint nn = *(pos++);
				//the following diagrams explain the bit twiddling addressing what origin of byte goes where.
				//[origin(0-12)] -> [distination(0-12)]
				//[2] -> [0]
				//[1] -> [1]
				//[0] -> [2]
				//[5] -> [3]
				*new_pos = (cur & 0xFF00) << 16 | (cur & 0xFF0000) | (cur & 0xFF000000) >> 16 | (next & 0xFF0000) >> 16;
				new_pos++;
				//[4] -> [4]
				//[3] -> [5]
				//[8] -> [6]
				//[7] -> [7]
				*new_pos = (next & 0xFF) | (cur & 0xFF) << 16 | (nn & 0xFF000000) >> 16 | (next & 0xFF000000);
				new_pos++;
				//[6] -> [8]
				//[11] -> [9]
				//[10] -> [10]
				//[9] -> [11]
				*new_pos = (next & 0xFF00) << 16 | (nn & 0xFF) << 16 | (nn & 0xFF00) | (nn & 0xff0000) >> 16;
				new_pos++;
				new_pos++;
				pos++;
			}
			//cleanup.
			//go to the next line of source and target.
			pos = next_a;
			new_pos = next_n;
		}
	}

	/**
	* This is an internal implementation for image::colour_cvt
	*/
	void colour_cvt(image& source, image& target, rectangle area) {
		//condition for optimized convertion @see #rgb2bgr_opt_le(uint*, uint*,int32, int32, int32, int32, int32) or #rgb2bgr_opt_be(uint*, uint*,int32, int32, int32, int32, int32).
		if (not_all({ source.view.width % 4, target.view.width % 4, area.width % 4 })) {
			if (is_little_endian())
				rgb2bgr_opt_le(source, target, area);
			else
				rgb2bgr_opt_be(source, target, area);
		}
		else {
			//optimized generic algorithm.
			//put all variables to the register for immiadate access in cpu.
			register byte* data = source.getData<byte>();
			register byte* oData = target.getData<byte>();
			//index: the position in the source image.
			register uint index = (source.view.x + area.x + (source.view.y + area.y) * source.view.width) * 3;
			//new_index: the position in the target image.
			register uint new_index = target.view.x + target.view.y * target.view.width;
			//image_end: the position to stop converting in bytes.
			uint image_end = index + source.view.width * area.height * 3;
			//line_end: the end position of the current line.
			register uint line_end;
			//while there is still data to convert.
			while (index < image_end) {
				//copy the whole line to the target image.
				memcpy(oData + new_index, data + index, area.width * 3);
				//set the end position of this line.
				line_end = index + area.width * 3;
				//the next line in the source image.
				int next_i = index + source.view.width * 3;
				//the next line in the target image.
				int next_ni = new_index + target.view.width * 3;
				//while there is still data to copy in this line.
				while (index < line_end) {
					//flip the first and third channel.
					oData[new_index++] = data[index + 2];
					new_index++;
					oData[new_index++] = data[index];
					index += 3;
				}
				//cleanup.
				//go to the next line of source and target.
				index = next_i;
				new_index = next_ni;
			}
		}
	}

	/**
	* This function is a replacement for toString() in java.
	*/
	std::string rectangle::str() {
		std::stringstream stream;
		stream << "[" << x << ", " << y << ":" << width << "x" << height << "]";
		return stream.str();
	}

	/**
	* this function is an operator overload that adds a rectangle a string, used for convenience sake for errors and debuging.
	*/
	std::string operator+(std::string t, rectangle r) {
		return r.str() + t;
	}

	//The diconstructor of image, deletes the data if the image has ownership on it.
	image::~image() {
		if (ownership) delete data;
	}


	/**
	* This function is a built-in colour conversion function in the image sturct if the parameters are all legal calls #cvtColour static method.
	* It can convert BGR -> RGB image and RGB -> BGR.
	*/
	void image::colour_cvt(image& dst, rectangle area) {
		if (view >= area && dst.view >= area && dst.view >= view)
			img_opt::colour_cvt(*this, dst, area);
		else
			throw "Bad rectangle: " + area + "source: " + view + "target" + dst.view;
	}

	/**
	* This function gets the 'viewed' section in the image.
	*/
	std::vector<byte> image::getCropped() {
		std::vector<byte> img_data = std::vector<byte>(view.width * view.height);
		for (size_t i = 0; i < view.height; i++)
			memcpy(img_data.data() + view.x + (view.y + i) * view.width, this->data + i * view.width, view.width);
		return img_data;
	}
}